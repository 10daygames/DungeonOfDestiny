﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace Engine
{
    public class Item
    {
        public int ArmorBonus { get; set; }
        public int DamageBonus { get; set; }
        public int HitBonus { get; set; }
        public int HealthBonus { get; set; }
        public string Name { get; set; }
        public string Equipped { get; set; }
        public int type { get; set; } // 1 for item, 2 for weapon, 3 for armor
        public int HitPoints { get; set; }
        public bool isConsumable { get; set; } = false;
        List<string> attributes = new List<string>();

        public Item()
        {
            ArmorBonus = 0;
            DamageBonus = 0;
            HitBonus = 0;
            HealthBonus = 0;
            Name = "Nothing of Nothingness";
            type = 1;
            Equipped = "false";
            HitPoints = 0;

        }

        public Item(int id)
        {
            XDocument reader = XDocument.Load("Data/items.xml");
            IEnumerable<XElement> item = 
                (from itm in reader.Root.Descendants("Item")
                 where (int)itm.Attribute("id") == id
                 select itm);
            Name = item.Elements("Name").First().Value.ToString();
            ArmorBonus = int.Parse(item.Elements("ArmorBonus").First().Value.ToString());
            DamageBonus = int.Parse(item.Elements("DamageBonus").First().Value.ToString());
            HitBonus = int.Parse(item.Elements("HitBonus").First().Value.ToString());
            type = int.Parse(item.Elements("Type").First().Value.ToString());
            HitPoints = int.Parse(item.Elements("HitPoints").First().Value.ToString());
            isConsumable = bool.Parse(item.Elements("Consumable").First().Value.ToString());
            Equipped = "false";
        } 

        public string debug()
        {
            string about = "";
            about += "Armor Bonus: " + ArmorBonus + "\n";
            about += "Damage Bonus: " + DamageBonus + "\n";
            about += "Hit Bonus: " + HitBonus + "\n";
            about += "Health Bonus: " + HealthBonus + "\n";
            about += "Name: " + Name + "\n";
            about += "Equipped: " + Equipped + "\n";
            about += "Type: " + type + "\n";
            about += "Consumable: " + isConsumable + "\n";
            return about;
        }

    }
}
        