﻿using System;
using System.Xml;

namespace Properties
{
    class Settings
    {
        public int maxItems { get; set; }
        public int maxMonster { get; set; }
        public int maxLevel { get; }
        public int maxX { get; }
        public int maxY { get; }

        public Settings()
        {
            XmlReader reader = XmlReader.Create("Data/settings.xml");
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    switch (reader.Name.ToString())
                    {
                        case "mapMaxX":
                            maxX = int.Parse(reader.ReadString().Trim());
                            break;

                        case "mapMaxY":
                            maxY =  int.Parse(reader.ReadString().Trim());
                            break;


                        case "maxMonster":
                            maxMonster = int.Parse(reader.ReadString().Trim());
                            break;

                        case "maxLevel":
                            maxLevel = int.Parse(reader.ReadString().Trim());
                            break;

                        case "maxItems":
                            maxItems = int.Parse(reader.ReadString().Trim());
                            break;
                    }
                }
                
            }
        }

    }
}
