﻿using System;


namespace Engine
{
    class Player
    {
        public int hitPoints { get; set; }
        public int constitution { get; set; }
        public int intellegence { get; set; }
        public int AC { get; set; }
        public Inventory bag { get; set; }
        public bool hasItem { get; set; }
        public bool hasWeapon { get; set; }
        public bool hasArmor { get; set; }
        public bool isDead { get; set; }
        public string equippedItem { get; set; }
        public string equippedArmor { get; set; }
        public string equippedWeapon { get; set; }
        public string playerName { get; set; }
        public int wisdom { get; set; }
        public int strength { get; set; }
        public int dexterity { get; set; }

        // rand used to set players starting skill levels. 
        Random rand = new Random();

        public Player(String name)
        {
            
            AC = 10;
            constitution = rand.Next(10, 15);
            hitPoints = constitution;
            intellegence = rand.Next(1, 6);
            wisdom = rand.Next(1, 6);
            strength = rand.Next(6, 11);
            dexterity = rand.Next(5, 8);
            playerName = name;
            bag = new Inventory();
            equippedItem = "none";
            equippedWeapon = "none";
            equippedArmor = "none";
            isDead = false;
        }

        public void kill(bool value)
        {
            isDead = value;
        }

        public void checkHP()
        {
            if (hitPoints > constitution)
            {
                int diff = hitPoints - constitution;
                hitPoints -= diff;
            }
            else
            {
            }
        }

        /// <summary>
        /// A Debug method that returns all of the data about a player. Used for tracking down issues. 
        /// </summary>
        /// <returns>About: a string with all of the information regarding a character.</returns>
        public string getCharacter()
        {
            string about = "";
            about += "Name: " + playerName + "\n";
            about += "HP: " + hitPoints+ "\n";
            about += "AC: " + AC + "\n";
            about += "Strength: " + strength + "\n";
            about += "Dexterity: " + dexterity + "\n";
            about += "Constitution: " + constitution + "\n";
            about += "Item: " + equippedItem + "\n";
            about += "Weapon: " + equippedWeapon + "\n";
            about += "Armor: " + equippedArmor + "\n";
            return about;
        }

    }
}
